using LinearAlgebra
using ToeplitzMatrices
using FFTW
using BlockArrays
using TensorCast

include("capture_obj.jl")
include("waveforms.jl")

function xcorr_fft(a::Capture, b::Capture; lags::Vector{<:Integer}=collect(-max(length(a),length(b)) + 1:max(length(a),length(b)) - 1))
    return xcorr_fft(a.val, b.val, lags=lags)
end


function xcorr_fft(a::Vector{<:Real}, b::Vector{<:Real}; lags::Vector{<:Integer}=collect(-max(length(a),length(b)) + 1:max(length(a),length(b)) - 1))
    # Ensure that `a` and `b` are of the same length for correlation calculation
    len_diff = length(a) - length(b)
    if len_diff > 0
        b = append!(zeros(-len_diff), b)
    elseif len_diff < 0
        a = append!(zeros(len_diff), a)
    end
    # Compute the size for zero-padding
    conv_length = length(a) + length(b) - 1
    nfft = nextpow(2, conv_length)

    # Zero-padding to the next power of 2 for efficient FFT
    af = fft([a; zeros(nfft - length(a))])
    bf = fft([b; zeros(nfft - length(b))])

    # Compute the cross-correlation
    c = ifft(af .* conj(bf))

    #Rotate so that the zero lag is in the middle
    c = fftshift(c)

    #Remove the zero-padding
    total_zero_padding = nfft - conv_length
    positive_padding = total_zero_padding ÷ 2
    negative_padding = total_zero_padding - positive_padding
    c = Real.(real.(c[negative_padding + 1:end - positive_padding]))

    # Generate index vector
    idx = collect(-length(a) + 1:length(a) - 1)

    # Find the index of lags in the index vector
    idx_lags = findall(in(lags), idx)

    # Extract the cross-correlation values for the specified lags
    c = c[idx_lags]

    if length(c) == 1
        return c[1]
    else
        return c
    end
end


"""
    wiener_coeff(u::Vector{<:Real}, y::Vector{<:Real}, order)

    u is an Nx1 vector representing the desired signal
    y is an NxM matrix of measurements where N is the number of samples, and M is the number of channels,
    order is the order of the filters generated.
"""
function wiener_coeff_multichannel(u::Vector{<:Real}, y::Matrix{<:Real}, order; regularization::Real=0, output_cond::Bool=false)

    M = size(y, 2)  # Number of channels
    lags = collect(0:order)

    # Calculate cross-correlation between u and y_1, y_2, ..., y_M
    @cast ry[i,j] |= xcorr_fft(u[:], y[:,j], lags=lags)[i]
    p = vcat(ry...)

    toeplitz(x) = Toeplitz(x, x) # Helper function
    if order == 0
        R = toeplitz([xcorr_fft(y[:, 1], y[:, 1], lags=[0])])
    else
        R = transpose(Matrix(mortar([toeplitz(xcorr_fft(y[:, i], y[:, j], lags=lags)) for i in 1:M, j in 1:M])))
    end

    pre_reg_cond = cond(R)
    # Add regularization
    R = R + regularization * I(size(R, 1))

    post_reg_cond = cond(R)

    # Solve the Wiener-Hopf equation for filter coefficients
    w = R \ p
    
    w = reshape(w, length(lags), M) # Reshape w to have 'order + 1' rows and 'M' columns

    if output_cond
        return w, pre_reg_cond, post_reg_cond
    else
        return w
    end 
end

# function wiener_coeff_multichannel(u::TimeDomain, y::Vector{TimeDomain}, order; regularization::Real=0, output_cond::Bool=false)
#     y_mat = reduce(hcat,[i.val for i in y])
#     return wiener_coeff_multichannel(u.val, y_mat, order, regularization=regularization, output_cond=output_cond)
# end

function wiener_coeff_multichannel(u::Capture, y::Vector{Capture{T}}, order; regularization::Real=0, output_cond::Bool=false) where T
    t = u.val
    y_mat = reduce(hcat,[i.val for i in y])
    return wiener_coeff_multichannel(t, y_mat, order, regularization=regularization, output_cond=output_cond)
end

# function wiener_coeff_multichannel(u::Multisine, y::Vector{Capture}, order; regularization::Real=0, output_cond::Bool=false)
#     t = waveform_to_array(u, length(y[1]))
#     y_mat = reduce(hcat,[i.response.val for i in y])
#     return wiener_coeff_multichannel(t, y_mat, order, regularization=regularization, output_cond=output_cond)
# end

function cv2cm(y, lags)
    zero_lag_idx = findfirst(==(0), lags)
    l = 1 + (length(y) - 1) ÷ 2
    R = zeros(l, l)
    for i = 0:l-1
        R[i+1, :] = y[zero_lag_idx-i:zero_lag_idx+l-i-1]
    end
    return R
end

function wiener_non_causal_matrix(u::Capture, y::Capture, order)
    M = 1  # Number of channels
    #lags = collect(0:length(u) - 1)
    lags_uy = collect(-(order-1):(order-1))
    lags = collect(0:order)
    # Calculate cross-correlation between u and y_1, y_2, ..., y_M
    ruy = xcorr_fft(u, y, lags=lags_uy)

    Ruy = cv2cm(ruy, lags_uy)'

    toeplitz(x) = Toeplitz(x, x) # Helper function
    Ryy = toeplitz(xcorr_fft(y, y, lags=lags))

    # Solve the Wiener-Hopf equation for filter coefficients
    #Ruy = toeplitz(p);
    w = Ryy\Ruy;

    #w = Ryy \ p
    
    #w = reshape(w, length(lags), M) # Reshape w to have 'order + 1' rows and 'M' columns

    return w

end
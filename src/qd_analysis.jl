module qd_analysis

export single_sided_fft
export welch_psd
include("calc_FFT.jl")

# export mse
# export rmse
# export interp
# export mse_mag
# export mse_phase
# include("mse.jl")

# # export extract_periodogram
# export extract_frequency_response
# export hilbert
# include("extract_complex_amplitude.jl")




export Waveform
export Sine
export Multisine
export is_periodic
export get_phase_at_sample
export waveform_to_array
export update_phase_for_sample
include("waveforms.jl")

export Capture
export qd_filt
export qd_matrix_mult
export identify_gap_free_ranges
export capture_to_gap_free_capture
include("capture_obj.jl")

export FrequencyResponse
include("frequencyResponse.jl")

export STFT
export stft_time
include("STFT.jl")

export split
include("split_types.jl")

export unwrap_sawtooth
export find_large_gaps
export nonuniform_to_uniform_sampling
include("nonuniform_sampling.jl")

export extract_frequency_response
include("extract_complex_amplitude.jl")
# export TimeDomain
# export FrequencyResponse
# export STFT
# export Excitation
# export Capture
# export acquisition_to_capture
# export cov
# export cor
# export autocor
# export crosscor
# export qd_filt
# include("processed_types.jl")

# export gen_sines
# export gen_multisine
# export gen_timedomain
# export gen_excitation
# export gen_capture
# export random_sine
# export random_multisine
# export waveform_to_timedomain
# export waveform_to_excitation
# export random_capture
# export capture_to_timedomain
# include("variable_generator.jl")

export wiener_coeff_multichannel
export xcorr_fft
export wiener_non_causal_matrix
include("wiener_filt.jl")

end # module qd_analysis

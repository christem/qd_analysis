using StructArrays
using TensorCast
using DocStringExtensions
using Random

"""
        General abstract type for all supported waveforms
"""
abstract type Waveform end


"""
       Encodes a sine wave

       $(TYPEDFIELDS)
"""
struct Sine
    #The normalized frequency of the sine wave
    frequency::Real
    #Phase in radians
    phase::Real
    #Unitless amplitude - usually normalized with respect to the full scale of an output/input stage
    amplitude::Real
end

function Base.:(==)(T::Sine, x::Sine)
    return T.frequency == x.frequency && T.phase == x.phase && T.amplitude == x.amplitude
end

"""
    $(SIGNATURES)

    returns the phase of a sinewave, sine, at sample number, trigger_idx.
"""
function get_phase_at_sample(sine::Sine, trigger_idx::Integer)
    return mod(sine.phase + sine.frequency * (trigger_idx) *2*π, 2π)
end

"""
       Encodes a multisine waveform (sum of sinusoids)

       $(FIELDS)
"""
struct Multisine <: Waveform
    #Array of sines
    sines::StructVector{Sine}
    #Length of waveform
    total_length::Integer
end

"""
    Multisine(frequencies::Vector{Real}, total_length::Integer, phases::Vector{Real}=zeros(length(frequencies)), amplitudes::Vector{Real}=ones(length(frequencies)))

Constructor for creating a `Multisine` with an array of frequencies. Phase and amplitude are optional parameters, defaulting to 0 and 1 respectively.
"""
function Multisine(frequencies::Vector{<:Number}; total_length::Integer=reduce(lcm, round.(Int, 1 ./ frequencies)), phases::Vector{<:Number}=zeros(length(frequencies)), amplitudes::Vector{<:Number}=ones(length(frequencies)))
    sines = StructVector(Sine.(frequencies, phases, amplitudes))
    return Multisine(sines, total_length)
end

function Base.:(==)(T::Multisine, x::Multisine)
    return T.sines == x.sines && T.total_length == x.total_length
end

"""
    $(SIGNATURES)

    Accepts a multisine or sine as an input. Returns true if every sine is periodic in its cycle length, false otherwise.
"""
function is_periodic(waveform::Multisine)
    @cast periodic_vec[i] |= is_periodic(waveform.sines[i], waveform.total_length)
    return all(periodic_vec)
end

function is_periodic(waveform::Sine, length::Integer)
    wave = waveform_to_array(waveform, length+1)
    return isapprox(wave[1], wave[end], atol=1e-9)
end


function waveform_to_array(wave::Sine, len::Integer)
    # Use a range directly instead of collecting into an array to avoid unnecessary allocations
    sample_times = 0:(len-1)
    # Calculate the sine wave directly using broadcasting and avoid intermediate variable allocations
    return wave.amplitude * cos.(2π * wave.frequency .* sample_times .+ wave.phase)
end


"""
    waveform_to_array(wave::Multisine)

    Accepts a multisine encoding as an input. Returns a vector containing the multisine wave.
"""
function waveform_to_array(wave::Multisine; len::Integer = wave.total_length, normalize::Bool=true)
    # Preallocate the result vector to avoid resizing during the sum operation
    result = zeros(Float64, len)
    
    for sine in wave.sines
        # Accumulate the individual sine waves directly into the result
        result .+= waveform_to_array(sine, len)
    end
    
    # Normalize the resulting multisine wave
    return normalize ? result ./ maximum(abs.(result)) : result
end


function update_phase_for_sample(waveform::Multisine, trigger_idx::Integer)
    triggered_phase = get_phase_at_sample.(waveform.sines, trigger_idx)
    sines = StructVector(Sine.(waveform.sines.frequency, triggered_phase, waveform.sines.amplitude))
    return Multisine(sines, waveform.total_length)
end
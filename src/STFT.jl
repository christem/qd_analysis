using StatsBase
using Statistics
include("frequencyResponse.jl")
"""
    STFT(frequency, time, response)
Encodes a short time fourier transform

# Fields
$(TYPEDFIELDS)
"""
struct STFT <: AbstractMatrix{Tuple{Real, Real, Complex}}
    frequency::Vector{<:Real}
    time_start::Real
    sample_rate::Real
    response::Matrix{<:Complex}

    function STFT(frequency::Vector{<:Real}, time_start::Real, sample_rate::Real, response::Matrix{<:Complex})
        if length(frequency) != size(response, 1)
            throw(DimensionMismatch("The dimensions of the frequency vector and response matrix are not consistent."))
        end
        new(frequency, time_start, sample_rate, response)
    end
end

"""
    STFT(freq_responses::Array{FrequencyResponse}, times::Vector{<:Real})

Generate a Short Time Fourier Transform (STFT) from an array of FrequencyResponse objects and a vector of times.

# Arguments
- `freq_responses::Array{FrequencyResponse}`: An array of FrequencyResponse objects. All FrequencyResponse objects should have the same frequency.
- `times::Vector{<:Real}`: A vector of times. The length of this vector should be the same as the length of `freq_responses`.

# Returns
- `STFT`: A Short Time Fourier Transform (STFT) object.
"""
function STFT(freq_responses::Array{FrequencyResponse}, times::Vector{<:Real})
    # Check that all FrequencyResponse objects have the same frequency
    freqs = [fr.frequency for fr in freq_responses]
    if !all([f == freqs[1] for f in freqs])
        throw(ArgumentError("All FrequencyResponse objects should have the same frequency."))
    end


    # Generate the response matrix
    response = hcat([fr.response for fr in freq_responses]...)

    sample_rate = 1/mode(diff(times))
    # Return the STFT
    return STFT(freqs[1], times[1], sample_rate, response)
end

function stft_time(ST::STFT)
    return collect(1:size(ST.response)[2]) * (1 / ST.sample_rate) .+ ST.time_start
end

# Find the nearest index for a given frequency value
function find_nearest_frequency_index(ST::STFT, frequency::Real)
    idx = findmin(abs.(ST.frequency .- frequency))[2]
    return idx
end

Base.size(ST::STFT) = size(ST.response)

# Find the nearest index for a given time value
function find_nearest_time_index(ST::STFT, time::Real)
    relative_time = time - ST.time_start
    idx = round(Int, relative_time * ST.sample_rate) + 1
    return clamp(idx, 1, size(ST.response, 2))
end

# Helper functions to find the nearest frequency and time indices
function find_nearest_frequency_index(ST::STFT, frequency::Real)
    idx = findmin(abs.(ST.frequency .- frequency))[2]
    return idx
end

function find_nearest_time_index(ST::STFT, time::Real)
    relative_time = time - ST.time_start
    idx = round(Int, relative_time * ST.sample_rate) + 1
    return clamp(idx, 1, size(ST.response, 2))
end

function find_nearest_frequency_indices(ST::STFT, freq_range::Tuple{Real, Real})
    start_idx = find_nearest_frequency_index(ST, freq_range[1])
    end_idx = find_nearest_frequency_index(ST, freq_range[2])
    return start_idx:end_idx
end

function find_nearest_time_indices(ST::STFT, time_range::Tuple{Real, Real})
    start_idx = find_nearest_time_index(ST, time_range[1])
    end_idx = find_nearest_time_index(ST, time_range[2])
    return start_idx:end_idx
end

# Frequency and time indexing
function Base.getindex(ST::STFT, freq_idx::Int, time_idx::Int)
    time = ST.time_start + (time_idx - 1) / ST.sample_rate
    return (ST.frequency[freq_idx], time, ST.sample_rate, ST.response[freq_idx, time_idx])
end

# Frequency (abs) and time indexing
function Base.getindex(ST::STFT, freq::Real, time_idx::Int)
    time = ST.time_start + (time_idx - 1) / ST.sample_rate
    freq_idx = find_nearest_frequency_index(ST, freq)
    return (ST.frequency[freq_idx], time, ST.sample_rate, ST.response[freq_idx, time_idx])
end

# Frequency (abs) and time (abs) indexing
function Base.getindex(ST::STFT, frequency::Real, time::Real)
    freq_idx = find_nearest_frequency_index(ST, frequency)
    time_idx = find_nearest_time_index(ST, time)
    return (ST.frequency[freq_idx], ST.time_start + (time_idx - 1) / ST.sample_rate, ST.sample_rate, ST.response[freq_idx, time_idx])
end

# Frequency and time (abs) indexing
function Base.getindex(ST::STFT, freq_idx::Int, time::Real)
    time_idx = find_nearest_time_index(ST, time)
    time_rounded = ST.time_start + (time_idx - 1) / ST.sample_rate
    return (ST.frequency[freq_idx], time_rounded, ST.sample_rate, ST.response[freq_idx, time_idx])
end


function Base.getindex(ST::STFT, freq_idx::Union{Tuple{Real, Real},Colon,UnitRange{Int}}, time_idx::Integer)
    if(isa(freq_idx, Tuple{Real, Real}))
        freq_idx = find_nearest_frequency_indices(ST, freq_idx)
    end
    frq = ST.frequency[freq_idx]
    response = ST.response[freq_idx, time_idx]
    return FrequencyResponse(frq, response) 
end

function Base.getindex(ST::STFT, freq_idx::Union{Tuple{Real, Real},Colon,UnitRange{Int}}, time::Real)
    if(isa(freq_idx, Tuple{Real, Real}))
        freq_idx = find_nearest_frequency_indices(ST, freq_idx)
    end
    frq = ST.frequency[freq_idx]
    response = ST.response[freq_idx, find_nearest_time_index(ST, time)]
    return FrequencyResponse(frq, response) 
end

function Base.getindex(ST::STFT, freq_idx::Integer, time_idx::Union{Tuple{Real, Real},Colon,UnitRange{Int}}) 
    if(isa(time_idx, Tuple{Real, Real}))
        time_idx = find_nearest_time_indices(ST, time_idx)
    end
    response = ST.response[freq_idx, :]
    tmp = Capture(response, ST.sample_rate, ST.time_start, nothing) # Capture
    return tmp[time_idx]
end

function Base.getindex(ST::STFT, frequency::Real, time_idx::Union{Tuple{Real, Real},Colon,UnitRange{Int}}) 
    if(isa(time_idx, Tuple{Real, Real}))
        time_idx = find_nearest_time_indices(ST, time_idx)
    end
    response = ST.response[find_nearest_frequency_index(ST, frequency), :]
    tmp = Capture(response, ST.sample_rate, ST.time_start, nothing) # Capture
    return tmp[time_idx]
end

function Base.getindex(ST::STFT, freq_idx::Union{Tuple{Real, Real},Colon,UnitRange{Int}}, time_idx::Union{Tuple{Real, Real},Colon,UnitRange{Int}}) 
    if(isa(freq_idx, Tuple{Real, Real}))
        freq_idx = find_nearest_frequency_indices(ST, freq_idx)
    end
    if(isa(time_idx, Tuple{Real, Real}))
        time_idx = find_nearest_time_indices(ST, time_idx)
    end
    if isa(time_idx, Colon)
        first_idx = 1
    else
        first_idx = first(time_idx)  # Get the first index of the range
    end
    # Calculate the new starting absolute time, factoring in the index offset and sample rate
    new_time_start = ST.time_start + ((first_idx - 1) * (1 / ST.sample_rate))
    return STFT(ST.frequency[freq_idx], new_time_start, ST.sample_rate, ST.response[freq_idx, time_idx])
end

# Overriding getindex for range indexing
function Base.getindex(ST::STFT, freq_idx::Union{Colon,StepRange{Int}}, time_idx::StepRange{Int}) where T
    
    if isa(time_idx, Colon)
        first_idx = 1
        step_size = 1
    else
        first_idx = first(time_idx)  # Get the first index of the range
        step_size = step(time_idx)   # Determine the step size
    end

    # Calculate the new starting absolute time, factoring in the index offset and sample rate
    new_time_start = ST.time_start + ((first_idx - 1) * (1 / ST.sample_rate))
    # Adjust the sample rate based on the step size
    # A larger step size means fewer samples per unit time, effectively reducing the sample rate
    new_sample_rate = ST.sample_rate / step_size

    # Create a new Capture object with the indexed values, adjusted time_start, and adjusted sample_rate
    return STFT(ST.frequency[freq_idx], new_time_start, new_sample_rate, ST.response[freq_idx, time_idx])
end


function Base.getindex(ST::STFT, freq_idx::AbstractVector{Int}, time_idx::Union{Tuple{Real, Real}, Colon, UnitRange{Int}, AbstractVector{Int}})
    if isa(time_idx, Tuple{Real, Real})
        time_idx = find_nearest_time_indices(ST, time_idx)
    elseif isa(time_idx, Real)
        time_idx = [find_nearest_time_index(ST, time_idx)]
    end
    frq = ST.frequency[freq_idx]
    response = ST.response[freq_idx, time_idx]
    # Compute new time_start based on the first time index
    if isa(time_idx, AbstractVector{Int}) || isa(time_idx, UnitRange{Int})
        new_time_start = ST.time_start + ((first(time_idx) - 1) / ST.sample_rate)
    else
        new_time_start = ST.time_start
    end
    return STFT(frq, new_time_start, ST.sample_rate, response)
end

function Base.setindex!(ST::STFT, value, freq_idx::Int, time_idx::Int)
    ST.response[freq_idx, time_idx] = value[3] # Update the response value
    return value
end

Base.eltype(::Type{STFT}) = Tuple{Real, Real, Real, Complex}

Base.axes(ST::STFT) = axes(ST.response)

Base.length(ST::STFT) = length(ST.response)

function Base.iterate(ST::STFT, state=(1,1))
    freq_len, time_len = size(ST)
    freq_idx, time_idx = state
    if freq_idx > freq_len
        return nothing
    else
        time = ST.time_start + (time_idx - 1) / ST.sample_rate
        next_state = time_idx < time_len ? (freq_idx, time_idx + 1) : (freq_idx + 1, 1)
        return ((ST.frequency[freq_idx], time, ST.sample_rate, ST.response[freq_idx, time_idx]), next_state)
    end
end

Base.similar(ST::STFT, ::Type{T}, dims::Dims) where {T<:Tuple{Real, Real, Complex}} =
    STFT(Vector{Real}(undef, dims[1]), ST.time_start, ST.sample_rate, Matrix{Complex}(undef, dims[1], dims[2]))

function Base.show(io::IO, ::MIME"text/plain", ST::STFT)
    freq_range = isempty(ST.frequency) ? "Empty" : "$(first(ST.frequency)) Hz - $(last(ST.frequency)) Hz"
    num_time_bins = size(ST.response, 2)
    time_end = ST.time_start + (num_time_bins - 1) / ST.sample_rate
    time_range = "from $(ST.time_start) s to $time_end s at a sample rate of $(ST.sample_rate) Hz"
    dim_response = size(ST.response)
    
    println(io, "STFT: Short Time Fourier Transform")
    println(io, "Frequency Range: $freq_range")
    println(io, "Time Range: $time_range")
    println(io, "Response Matrix Dimensions: $(dim_response[1]) Frequency Bins x $(dim_response[2]) Time Bins")
    if !isempty(ST.response)
        min_val = minimum(abs.(ST.response))
        max_val = maximum(abs.(ST.response))
        println(io, "Response Magnitude Range: $min_val - $max_val")
    else
        println(io, "Response: Empty")
    end
end

function Statistics.mean(ST::STFT)
    return mean([ST[:, k] for k in 1:size(ST.response, 2)])
end

import Base: +, -, *, /

# Overload addition for STFT objects
function Base.:+(ST1::STFT, ST2::STFT)
    if !(all(ST1.frequency .== ST2.frequency))
        throw(DimensionMismatch("STFT objects must have the same frequencies and times to be added."))
    end
    return STFT(ST1.frequency, ST1.time_start, ST1.sample_rate, ST1.response .+ ST2.response)
end

# Overload subtraction for STFT objects
function Base.:-(ST1::STFT, ST2::STFT)
    if !(all(ST1.frequency .== ST2.frequency))
        throw(DimensionMismatch("STFT objects must have the same frequencies and times to be subtracted."))
    end
    return STFT(ST1.frequency, ST1.time_start, ST1.sample_rate,  ST1.response .- ST2.response)
end

# Overload multiplication for STFT objects
function Base.:*(ST1::STFT, ST2::STFT)
    if !(all(ST1.frequency .== ST2.frequency))
        throw(DimensionMismatch("STFT objects must have the same frequencies and times to be multiplied."))
    end
    return STFT(ST1.frequency, ST1.time_start, ST1.sample_rate, ST1.response .* ST2.response)
end

# Overload division for STFT objects
function Base.:/(ST1::STFT, ST2::STFT)
    if !(all(ST1.frequency .== ST2.frequency))
        throw(DimensionMismatch("STFT objects must have the same frequencies and times to be divided."))
    end
    return STFT(ST1.frequency, ST1.time_start, ST1.sample_rate, ST1.response ./ ST2.response)
end

# Overload operations between STFT objects and scalars
for op in (:+, :-, :*, :/)
    @eval begin
        function Base.$op(ST::STFT, num::Number)
            return STFT(ST.frequency, ST.time_start, ST.sample_rate, $op.(ST.response, num))
        end
        function Base.$op(num::Number, ST::STFT)
            return STFT(ST.frequency, ST.time_start, ST.sample_rate, $op.(num, ST.response))
        end
    end
end

# Overload operations between STFT objects and matrices
for op in (:+, :-, :*, :/)
    @eval begin
        function Base.$op(ST::STFT, mat::AbstractMatrix)
            if size(ST.response) != size(mat)
                throw(DimensionMismatch("STFT response matrix and input matrix must have the same dimensions."))
            end
            return STFT(ST.frequency, ST.time_start, ST.sample_rate, $op.(ST.response, mat))
        end
        function Base.$op(mat::AbstractMatrix, ST::STFT)
            if size(ST.response) != size(mat)
                throw(DimensionMismatch("Input matrix and STFT response matrix must have the same dimensions."))
            end
            return STFT(ST.frequency, ST.time_start, ST.sample_rate, $op.(mat, ST.response))
        end
    end
end

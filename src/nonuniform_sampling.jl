using BSplineKit
using Statistics

function next_power_of_two(x)
    if x <= 0
        return 1
    else
        return 2^(ceil(log2(x)))
    end
end

function unwrap_sawtooth(array, wrap_point)
    unwrapped_array = copy(array)  # Make a copy to preserve the original data
    for i in 2:length(array)
        if array[i] < array[i-1] # If the phase jumps backward
            unwrapped_array[i:end] .+= wrap_point*2
        end
    end
    return unwrapped_array
end

function find_large_gaps(sample_arr, threshold_inner=20, threshold_outer=1000)
    diffs = diff(sample_arr)
    gap_indices = [(i, diffs[i]) for i in 1:length(diffs) if diffs[i] > threshold_inner]

    merged_gaps = []
    i = 1

    old_end = 1
    while i <= length(gap_indices)
        start = i
        # Initialize end as start; it will move forward if gaps are within the threshold_outer
        _end = start

        # Look ahead to merge close gaps
        while _end < length(gap_indices) && (gap_indices[_end + 1][1] - gap_indices[start][1]) < threshold_outer
            _end += 1
        end

        # Calculate the actual difference using the start and end of the merged gap
        start_idx = gap_indices[start][1]
        end_idx = gap_indices[_end][1] + 1 # +1 because diff reduces the index by 1
        total_difference = sample_arr[end_idx] - sample_arr[start_idx]

        push!(merged_gaps, (old_end:start_idx, total_difference))
        old_end = end_idx
        i = _end + 1
    end

    push!(merged_gaps, (old_end:length(sample_arr), 0))

    return merged_gaps
end

function nonuniform_to_uniform_sampling(input::Vector{<:Real},sample_ids::Vector{<:Real}; wrap_point::Integer = 0, stride::Integer =0)
    # if wrap_point == 0
    #     wrap_point = next_power_of_two(maximum(sample_ids))
    # end
    
    # sample_ids_unwrapped = unwrap_sawtooth(sample_ids, wrap_point)

    itp = BSplineKit.SplineInterpolations.interpolate(sample_ids, input, BSplineOrder(3))

    #interp_linear = linear_interpolation(sample_ids_unwrapped, input)

    if stride == 0
        stride = round(mean(diff(sample_ids)))
    end

    return itp.(collect(minimum(sample_ids):stride:maximum(sample_ids)))
end

function nonuniform_to_uniform_sampling(input::Capture,sample_ids::Vector{<:Real}; wrap_point::Integer = 0, stride::Integer = 0)
    return Capture(nonuniform_to_uniform_sampling(input.val, sample_ids, wrap_point = wrap_point, stride = stride), input.sample_rate, input.time_start, input.stimuli)
end

function nonuniform_to_uniform_sampling(input::Capture,sample_ids::Capture; wrap_point::Integer = 0, stride::Integer = 0)
    return nonuniform_to_uniform_sampling(input, sample_ids.val, wrap_point = wrap_point, stride = stride)
end
using DSP
using FFTW
using LinearAlgebra
using JLD2
using JSON3
using TensorCast
using Interpolations
using FileIO
using JLD2
using StructArrays
using SHA
using Optim

include("waveforms.jl")
include("split_types.jl")
include("windows.jl")
include("frequencyResponse.jl")
include("capture_obj.jl")

A_qr_dict = Dict{String, Any}()

function filter_threshold(T::Capture, threshold::Real)
  mask = abs.(T.val) .<= threshold
  return T[mask]
end


function extract_frequency_response(capture_vec::Vector{Capture{T}}; A_qr::Any = nothing, window::Symbol=:Rectangular, peak_threshold::Number=0.0) where T
  results = extract_frequency_response.(capture_vec, A_qr=Ref(A_qr), window=window, peak_threshold=peak_threshold)
  timevec = [i.time_start for i in capture_vec]
  return STFT(results, timevec)
end

function extract_frequency_response(capture_vec::Vector{Capture{T}}, waveform::Multisine; A_qr::Any = nothing, window::Symbol=:Rectangular, peak_threshold::Number=0.0) where T
  results = extract_frequency_response.(capture_vec, Ref(waveform), A_qr=Ref(A_qr), window=window, peak_threshold=peak_threshold)
  timevec = [i.time_start for i in capture_vec]
  return STFT(results, timevec)
end


function extract_frequency_response(capture::Capture; A_qr::Any = nothing, window::Symbol=:Rectangular, peak_threshold::Number=0.0, amplitudes_to_return::Symbol=:biggest, noise_signal::Union{Vector{<:AbstractFloat}, Nothing}=nothing, filter_length::Int=1)
  if isnothing(capture.gap_free_ranges)
    return extract_frequency_response(capture, capture.stimuli, A_qr = Ref(A_qr), window=window, noise_signal=noise_signal, filter_length=filter_length)
  else
    if amplitudes_to_return == :combined || amplitudes_to_return == :all
      captures = capture_to_gap_free_capture(capture)
      ffts = extract_frequency_response.(captures, A_qr = A_qr, window=window, peak_threshold=peak_threshold)

      if amplitudes_to_return == :combined
        capture_lengths = [length(i) for i in captures]
        total_length = sum(capture_lengths)
        weights = [length(i) / total_length for i in captures]

        weighted_fft = abs.(weights .* ffts) # Required to take abs as phase is not guarentted to be aligned -TODO TO ALIGN PHASE LATER
        return sum(weighted_fft)
      elseif amplitudes_to_return == :all
        return ffts
      end
    elseif amplitudes_to_return == :biggest
      biggest_capture = capture_to_gap_free_capture(capture, type=:biggest)
      fft = extract_frequency_response(biggest_capture, A_qr = A_qr, window=window, peak_threshold=peak_threshold, noise_signal=noise_signal, filter_length=filter_length)
      return fft
    else
      throw(ArgumentError("Invalid value for amplitudes_to_return"))
      
    end
  end
end

function extract_frequency_response(capture::Capture, waveform::Multisine; A_qr::Any = nothing, window::Symbol=:Rectangular, peak_threshold::Number=0.0, noise_signal::Union{Vector{<:AbstractFloat}, Nothing}=nothing, filter_length::Int=1)
  if peak_threshold != 0.0
    capture = filter_threshold(capture, peak_threshold)
  end
  if !isnothing(noise_signal)
    response, filter_coefs = extract_frequency_response(capture.val, collect(1:length(capture)), waveform, A_qr = Ref(A_qr), window=window, noise_signal=noise_signal, filter_length=filter_length)
    frequencies =  capture.sample_rate*waveform.sines.frequency
    return FrequencyResponse(frequencies, response), filter_coefs
  else
    response = extract_frequency_response(capture.val, collect(1:length(capture)), waveform, A_qr = Ref(A_qr), window=window)
    frequencies =  capture.sample_rate*waveform.sines.frequency
    response = FrequencyResponse(frequencies, response)
    return response
  end
end


function shifted_columns(a, x)
  n = length(a)
  M = zeros(n, x)
  for j in 1:x
      # Number of zeros to pad at the top
      pad_length = j - 1
      # Elements of 'a' to include in the column
      a_segment = a[1:(n - pad_length)]
      # Construct the column with zeros padded at the top
      M[:, j] = [zeros(pad_length); a_segment]
  end
  return M
end


function extract_frequency_response(capture::Vector{<:AbstractFloat}, samples::Vector{<:Number}, waveform::Multisine; A_qr::Any = nothing, window::Symbol=:Rectangular, noise_signal::Union{Vector{<:AbstractFloat}, Nothing}=nothing, filter_length::Int=1)
  win = get_window_function(window, length(capture))
  capture = capture .* win
  # Generate a unique hash based on the frequency array
  frequency_hash = bytes2hex(sha256(reinterpret(UInt8, Float64.([length(capture);waveform.sines.frequency]))))
  A_qr_file_path = "A_qr_$(frequency_hash).jld2"
  
  # Check if A_qr exists in memory with the correct hash
  if typeof(A_qr) == LinearAlgebra.QRCompactWY{ComplexF64, Matrix{ComplexF64}, Matrix{ComplexF64}}
    # A_qr is already in memory and matches the frequency_hash
    A_qr_mat = A_qr
  
  # elseif haskey(A_qr_dict, frequency_hash)
  #   A_qr_mat = A_qr_dict[frequency_hash]

  # # Check if A_qr exists in the local file directory with the correct hash
  # elseif isfile(A_qr_file_path)
  #   A_qr_mat = FileIO.load(A_qr_file_path, "A_qr")
  else
    # Compute A matrix and QR decomposition
    #N = collect(0:size(capture,1)-1
    N = samples .- 1
    ω = waveform.sines.frequency
    #A = @. exp(im*2*π*(N*ω'))
    A_c = @. cos(2*π*(N*ω'))
    A_s = @. sin(-2*π*(N*ω'))
    A = hcat(A_c, A_s) 
    if !isnothing(noise_signal)
      if length(noise_signal) != length(capture)
        throw(ArgumentError("Noise signal must be the same length as the capture"))
      end
      
      A = hcat(A, shifted_columns(noise_signal,filter_length))
    end

    #A = [@. cos(im*2*π*(N*ω')) @. sin(im*2*π*(N*ω'))]
    #A_qr_mat = qr(A)
    
    # if A_qr != false
    #   # Save A_qr to global Dict
    #   A_qr_dict[frequency_hash] = A_qr_mat
    #   # Save A_qr to a file
    #   FileIO.save(A_qr_file_path, Dict("A_qr" => A_qr_mat))
    # end
  end
  
  #x = A_qr_mat\DSP.hilbert(capture)
  #x = A_qr_mat\capture
  x = A\capture

  #x_complex = x[1:end÷2] + x[end÷2+1:end]*im
  x_complex = x[1:length(ω)] + x[1+length(ω):1+2*length(ω)-1]*im

  filter_coefs = x[1+2*length(ω):end]
  phase = exp.(waveform.sines.phase*im)
  x_complex = x_complex./phase

  # A_new = [real.(A) -imag.(A); imag.(A) real.(A)]
  # capture_new = [real.(hilbert(capture)); imag.(hilbert(capture))]

  if !isnothing(noise_signal)
    return x_complex, filter_coefs
  end
  return x_complex
end


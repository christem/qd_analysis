using FFTW
include("processed_types.jl")
include("windows.jl")
include("capture_obj.jl")


using FFTW
include("processed_types.jl")
include("windows.jl")

function ENBW(win=Vector{<:Number})
    return length(win) *sum(win.^2) / sum(win)^2
end

function welch_psd(cap::Capture{T}; window::Symbol=:Rectangular, segment_size::Integer=1024, overlap::Integer=512) where T
    split_cap = split(cap, segment_size, overlap)
    ffts = [single_sided_fft(i, window=window) for i in split_cap]
    psds = [(abs(i)*abs(i))/2 for i in ffts] #div by 2 to convert peak to RMS
    win = get_window_function(window, segment_size)
    e = ENBW(win)
    avg_psd = sum(psds) / length(psds)
    bin_width = cap.sample_rate / segment_size
    return avg_psd / (bin_width*e)
end


function single_sided_fft(capture_vec::Vector{Capture{T}}; window::Symbol=:Rectangular) where T
    results = single_sided_fft.(capture_vec, window=window)
    timevec = [i.time_start for i in capture_vec]
    return STFT(results, timevec)
end


function single_sided_fft(time_domain::Capture{T}; window::Symbol=:Rectangular, zero_padding::Integer=0, fft_to_return::Symbol=:biggest) where T

    if isnothing(time_domain.gap_free_ranges)
        return single_sided_fft(time_domain.val, time_domain.sample_rate, window=window, zero_padding=zero_padding)
    else
        if fft_to_return == :combined || fft_to_return == :all
            captures = capture_to_gap_free_capture(time_domain)
            if fft_to_return == :combined
                capture_lengths = [length(i) for i in captures]
                max_length = maximum(capture_lengths)
                zero_padding_array = [max_length - i for i in capture_lengths]
                ffts = map((c, zp) -> single_sided_fft(c, window=window, zero_padding=zp), captures, zero_padding_array)
                total_length = sum(capture_lengths)
                weights = [length(i) / total_length for i in captures]
                weighted_fft = abs.(weights .* ffts) # Required to take abs as phase is not guarentted to be aligned
                return sum(weighted_fft)
            elseif fft_to_return == :all
                ffts = single_sided_fft.(captures, window=window)
            end
        elseif fft_to_return == :biggest
            biggest_capture = capture_to_gap_free_capture(capture, type=:biggest)
            return single_sided_fft(biggest_capture, window=window)
        else
            throw(ArgumentError("Invalid value for fft_to_return"))
        end
    end
end



"""
    single_sided_fft(time_domain::TimeDomain, window::Function=RectangularWindow) -> FrequencyResponse

Calculate the single-sided FFT of an input TimeDomain type and return it as a FrequencyResponse type.

# Arguments
- `time_domain`: The TimeDomain object containing the samples.
- `window`: The windowing function to apply. Default is rectangular (no window).

# Returns
A FrequencyResponse object containing the frequency spectrum.
"""
function single_sided_fft(sample_array::Vector{<:Number}, sample_rate::Number ; window::Symbol=:Rectangular, zero_padding::Int=0)
    N = length(sample_array)
    
    # Generate the window of length N
    win = get_window_function(window, N)

    # Check if the length of the window matches the length of the signal
    if length(win) != N
        throw(DimensionMismatch("Length of the window does not match the length of the signal"))
    end

    # Apply the window to the time-domain signal
    windowed_signal = sample_array .* win

    # Apply zero-padding if required
    if zero_padding > 0
        windowed_signal = append!(windowed_signal, zeros(zero_padding))
        N = N + zero_padding
    end

    # Calculate FFT and normalize it by the sum of the window values (or adjust as required)
    fft_vals = fft(windowed_signal) / sum(win)

    # Calculate the frequency bins
    freq = range(0, step=sample_rate/N, length=div(N, 2) + 1)

    # Take the single-sided FFT
    single_sided_fft_vals = fft_vals[1:div(N, 2) + 1]

    # Multiply by 2 to account for the single-sided spectrum
    single_sided_fft_vals[2:end-1] .= 2 .* single_sided_fft_vals[2:end-1]

    return FrequencyResponse(collect(freq), single_sided_fft_vals)
end

"""
    FrequencyResponse(frequency, response)
Encodes a frequency domain capture

# Fields
$(TYPEDFIELDS)
"""
struct FrequencyResponse <: AbstractVector{Tuple{Real, Complex}}
    frequency::Vector{<:Real}
    response::Vector{<:Complex}
    function FrequencyResponse(frequency::Vector{<:Real}, response::Vector{<:Complex})
        if length(frequency) != length(response)
            throw(DimensionMismatch("The dimensions of the input vectors are not consistent."))
        end
        # Pair frequencies with responses, sort by frequency, then unzip
        sorted_pairs = sort(collect(zip(frequency, response)), by = first)
        new_frequency, new_response = unzip(sorted_pairs)
        new(new_frequency, new_response)
    end
end

# Helper function to unzip the sorted pairs back into two vectors
function unzip(pairs::Vector{<:Tuple{<:Real, <:Complex}})
    frequency = [pair[1] for pair in pairs]
    response = [pair[2] for pair in pairs]
    return frequency, response
end

Base.size(FR::FrequencyResponse) = size(FR.frequency)

Base.getindex(FR::FrequencyResponse, idx::Int) = (FR.frequency[idx], FR.response[idx])

Base.getindex(FR::FrequencyResponse, idx::UnitRange{Int}) = FrequencyResponse(FR.frequency[idx], FR.response[idx])

Base.getindex(FR::FrequencyResponse, idx::StepRange{Int, Int}) = FrequencyResponse(FR.frequency[idx], FR.response[idx])

function Base.setindex!(FR::FrequencyResponse, value::Tuple{Real, Complex}, idx::Int)
    # Check if the frequency value is actually being changed
    if FR.frequency[idx] != value[1]
        # Frequency value is changed, so update and resort
        FR.frequency[idx] = value[1]
        FR.response[idx] = value[2]

        # Re-sort the frequency and response vectors together
        sorted_pairs = sort(collect(zip(FR.frequency, FR.response)), by = first)
        new_frequency, new_response = unzip(sorted_pairs)

        # Update the vectors in the FrequencyResponse instance in place
        for i in 1:length(FR)
            FR.frequency[i] = new_frequency[i]
            FR.response[i] = new_response[i]
        end
    else
        # Only the response value is changed, no need to resort
        FR.response[idx] = value[2]
    end

    return value
end

Base.eltype(::Type{FrequencyResponse}) = Tuple{Real, Complex}

Base.axes(FR::FrequencyResponse) = axes(FR.frequency)

Base.length(FR::FrequencyResponse) = length(FR.frequency)

Base.iterate(FR::FrequencyResponse, state=1) = state > length(FR) ? nothing : ((FR.frequency[state], FR.response[state]), state + 1)

Base.similar(FR::FrequencyResponse, ::Type{S}, dims::Dims) where {S<:Tuple{Real, Complex}} = FrequencyResponse(Vector{Real}(undef, dims...), Vector{Complex}(undef, dims...))

# Overload maximum to return the maximum absolute response value
function Base.maximum(FR::FrequencyResponse)
    return maximum(abs.(FR.response))
end

# Overload minimum to return the minimum absolute response value
function Base.minimum(FR::FrequencyResponse)
    return minimum(abs.(FR.response))
end

# Overload argmax to return the index of the frequency with the maximum absolute response value
function Base.argmax(FR::FrequencyResponse)
    abs_responses = abs.(FR.response)
    return argmax(abs_responses)
end

# Overload argmin to return the index of the frequency with the minimum absolute response value
function Base.argmin(FR::FrequencyResponse)
    abs_responses = abs.(FR.response)
    return argmin(abs_responses)
end

import Base: +, -, *, /

# Overload addition for FrequencyResponse objects
function Base.:+(FR1::FrequencyResponse, FR2::FrequencyResponse)
    if length(FR1) != length(FR2) || any(FR1.frequency .!= FR2.frequency)
        throw(DimensionMismatch("FrequencyResponse objects must have the same frequencies to be added."))
    end
    return FrequencyResponse(FR1.frequency, FR1.response .+ FR2.response)
end

# Overload subtraction for FrequencyResponse objects
function Base.:-(FR1::FrequencyResponse, FR2::FrequencyResponse)
    if length(FR1) != length(FR2) || any(FR1.frequency .!= FR2.frequency)
        throw(DimensionMismatch("FrequencyResponse objects must have the same frequencies to be subtracted."))
    end
    return FrequencyResponse(FR1.frequency, FR1.response .- FR2.response)
end

# Overload multiplication for FrequencyResponse objects
function Base.:*(FR1::FrequencyResponse, FR2::FrequencyResponse)
    if length(FR1) != length(FR2) || any(FR1.frequency .!= FR2.frequency)
        throw(DimensionMismatch("FrequencyResponse objects must have the same frequencies to be multiplied."))
    end
    return FrequencyResponse(FR1.frequency, FR1.response .* FR2.response)
end

# Overload division for FrequencyResponse objects
function Base.:/(FR1::FrequencyResponse, FR2::FrequencyResponse)
    if length(FR1) != length(FR2) || any(FR1.frequency .!= FR2.frequency)
        throw(DimensionMismatch("FrequencyResponse objects must have the same frequencies to be divided."))
    end
    return FrequencyResponse(FR1.frequency, FR1.response ./ FR2.response)
end

# Overload addition, subtraction, multiplication, and division for FrequencyResponse objects and numbers
for op in (:+, :-, :*, :/)
    @eval begin
        function Base.$op(FR::FrequencyResponse, num::Number)
            return FrequencyResponse(FR.frequency, $op.(FR.response, num))
        end
        function Base.$op(num::Number, FR::FrequencyResponse)
            return FrequencyResponse(FR.frequency, $op.(num, FR.response))
        end
    end
end

# Overload addition, subtraction, multiplication, and division for FrequencyResponse objects and arrays
for op in (:+, :-, :*, :/)
    @eval begin
        function Base.$op(FR::FrequencyResponse, arr::AbstractArray)
            if length(FR) != length(arr)
                throw(DimensionMismatch("FrequencyResponse object and array must have the same length."))
            end
            return FrequencyResponse(FR.frequency, $op.(FR.response, arr))
        end
        function Base.$op(arr::AbstractArray, FR::FrequencyResponse)
            if length(FR) != length(arr)
                throw(DimensionMismatch("FrequencyResponse object and array must have the same length."))
            end
            return FrequencyResponse(FR.frequency, $op.(arr, FR.response))
        end
    end
end


function Base.abs(FR::FrequencyResponse)
    return FrequencyResponse(FR.frequency, abs.(FR.response) .+ 0im)
end

function Statistics.mean(FR::Vector{FrequencyResponse})
    return FrequencyResponse(FR[1].frequency, mean(vcat([FR[i].response for i in 1:length(FR)]), dims=1)[1])
end
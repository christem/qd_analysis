using Interpolations
using LinearAlgebra
using DSP
include("processed_types.jl")


function interp(y::FrequencyResponse, x::Vector{<:Real})

    interp_source  = linear_interpolation(y.frequency, y.response)

    interpped_response = interp_source(x)

    return interpped_response
end


function mse(y::FrequencyResponse, ŷ::FrequencyResponse)

    interpped_response = interp(y, ŷ.frequency)

    return sum(norm.(interpped_response - ŷ.response,2).^2) / length(ŷ.response)
end

function mse(y::FrequencyResponse, ŷ::STFT)

    interpped_response = interp(y, ŷ.frequency)

    mse_vec = vec(sum(norm.(interpped_response .- ŷ.response,2).^2, dims=1) ./ length(ŷ.frequency))
    return mse_vec
end

function rmse(y::FrequencyResponse, ŷ::STFT)

    interpped_response = interp(y, ŷ.frequency)
    
    return vec(sum(norm.((interpped_response .- ŷ.response)./interpped_response,2).^2, dims=1) ./ length(ŷ.frequency))
end


function mse_mag(y::FrequencyResponse, ŷ::FrequencyResponse)

    interpped_response = interp(y, ŷ.frequency)

    return sum(norm.(abs.(interpped_response) - abs.(ŷ.response),2).^2) / length(ŷ.response)
end

function mse_mag(y::FrequencyResponse, ŷ::STFT)

    interpped_response = interp(y, ŷ.frequency)

    mse_vec = vec(sum(norm.(abs.(interpped_response) .- abs.(ŷ.response),2).^2, dims=1) ./ length(ŷ.frequency))
    return mse_vec
end

function mse_phase(y::FrequencyResponse, ŷ::FrequencyResponse)

    interpped_response = interp(y, ŷ.frequency)


    #Unwrap phase difference
    a = [unwrap([angle(interpped_response[i]),angle(ŷ.response[i])]) for i in 1:length(ŷ.frequency)]

    return sum(norm.(rad2deg.(a[1] - a[2]),2).^2) / length(ŷ.response)
end

function mse_phase(y::FrequencyResponse, ŷ::STFT)

    interpped_response = interp(y, ŷ.frequency)

    diff_matrix = zeros(size(ŷ.response,1), size(ŷ.response,2))
    for i in 1:size(ŷ.response,2)
        for j in 1:size(ŷ.response,1)
            a = unwrap([angle.(interpped_response[j]), angle.(ŷ.response[j,i])])
            diff_matrix[j,i] = a[1] - a[2]
        end
    end

    mse_vec = vec(sum(norm.(rad2deg.(diff_matrix),2).^2, dims=1) ./ length(ŷ.frequency))

    return mse_vec
end
using Base: size, getindex, setindex!, eltype, axes
using Statistics
using DSP
include("waveforms.jl")

struct Capture{T<:Number} <: AbstractVector{T}
    val::Vector{T}
    sample_rate::Real
    time_start::Number
    stimuli::Union{Waveform, Nothing}
    gap_free_ranges::Union{Vector{UnitRange{Int}}, Nothing}
end



# Convenience constructor for when only a vector is provided. All other fields are optionally provided
Capture(val::Vector{T}; sample_rate = 1.0, time_start=0.0, stimuli = nothing) where T <: Number = Capture{T}(val, sample_rate, time_start, stimuli, identify_gap_free_ranges(val))

# Convenience constructor for when waveform is provided. The value array is generated from this.
Capture(stimuli::Waveform; sample_rate = 1.0, time_start=0.0, len::Integer=stimuli.total_length, normalize::Bool=true) = Capture{Float64}(waveform_to_array(stimuli, len=len, normalize=normalize), sample_rate, time_start, stimuli, nothing)

Capture(val::Vector{T}, sample_rate, time_start, stimuli) where T <: Number = Capture{T}(val, sample_rate, time_start, stimuli, identify_gap_free_ranges(val))

# Essential AbstractArray interface methods
Base.size(td::Capture) = size(td.val)

function capture_to_gap_free_capture(capture::Capture{T}; type::Symbol = :all) where T
    if isnothing(capture.gap_free_ranges)
        return capture
    end

    if type == :all
        return [capture[i] for i in capture.gap_free_ranges]
    else type == :biggest
        biggest_range = capture.gap_free_ranges[argmax([length(i) for i in capture.gap_free_ranges])]
        return capture[biggest_range]
    end
end

function identify_gap_free_ranges(val::Vector{<:Number})
    isnan_vector = map(!, isnan.(val))  # Vector indicating if each element is NaN
    
    if all(isnan_vector)  # If all elements are NaN, return nothing
        return nothing
    end

    non_nan_idx = findall(isnan_vector)
    
    # Edge case: Empty input
    isempty(non_nan_idx) && return UnitRange{Int}[]

    # Find the differences between consecutive elements
    diffs = diff(non_nan_idx)
    
    # Identify the end points of ranges (where difference is not 1)
    breaks = findall(diffs .!= 1)
    
    # Starting points of ranges are one after each break, plus the first index
    starts = [1; breaks .+ 1]
    
    # Ending points are the indices right before the next start, plus the last index
    ends = [breaks; length(non_nan_idx)]
    
    # Create the ranges
    ranges = [non_nan_idx[starts[i]]:non_nan_idx[ends[i]] for i in 1:length(starts)]

    return ranges
end

function identify_gap_free_ranges(cap::Capture{T}) where T
    gaps = identify_gap_free_ranges(cap.val)
    return Capture(cap.val, cap.sample_rate, cap.time_start, cap.stimuli, gaps)
end

# Overriding getindex for range indexing
function Base.getindex(td::Capture{T}, idx::StepRange{Int,Int}) where T
    first_idx = first(idx)  # Get the first index of the range
    step_size = step(idx)   # Determine the step size

    # Calculate the new starting absolute time, factoring in the index offset and sample rate
    new_time_start = td.time_start + ((first_idx - 1) * (1 / td.sample_rate))
    # Adjust the sample rate based on the step size
    # A larger step size means fewer samples per unit time, effectively reducing the sample rate
    new_sample_rate = td.sample_rate / step_size

    # Create a new Capture object with the indexed values, adjusted time_start, and adjusted sample_rate
    return Capture(td.val[idx], new_sample_rate, new_time_start, td.stimuli)
end

function Base.getindex(td::Capture{T}, idx::UnitRange{Int}) where T
    first_idx = first(idx)  # Get the first index of the range

    # Calculate the new starting absolute time, factoring in the index offset and sample rate
    new_time_start = td.time_start + ((first_idx - 1) * (1 / td.sample_rate))

    # Create a new Capture object with the indexed values, adjusted time_start, and adjusted sample_rate
    return Capture(td.val[idx], td.sample_rate, new_time_start, td.stimuli)
end


# Overriding getindex for a single index, returning an element of the array rather than a Capture object
function Base.getindex(td::Capture{T}, idx::Int) where T
    return td.val[idx]
end

Base.setindex!(td::Capture, value, i::Int) = (td.val[i] = value; td)

Base.eltype(::Type{Capture{T}}) where {T} = T

Base.axes(td::Capture) = axes(td.val)

# Optional but useful for performance and functionality
Base.length(td::Capture) = length(td.val)

Base.iterate(td::Capture, state=1) = state > length(td) ? nothing : (td.val[state], state + 1)

Base.similar(td::Capture, ::Type{T}, dims::Dims) where {T} = Capture(Vector{T}(undef, dims...), td.sample_rate, td.time_start, td.stimuli, td.gap_free_ranges)



### Mathematical Overloads:

# Addition of two Capture objects, ensuring matching sample rates and lengths
function Base.:+(T::Capture, x::Capture)
    if T.sample_rate != x.sample_rate || length(T) != length(x)
        throw(ArgumentError("Capture objects must have the same sample rate and length to be added."))
    end
    return Capture(T.val + x.val, T.sample_rate, T.time_start, T.stimuli)
end

# Addition of a Capture object and a Number, adding the Number to each element of the Capture's val
function Base.:+(T::Capture, x::Number)
    return Capture(T.val .+ x, T.sample_rate, T.time_start, T.stimuli)
end

# Addition of a Number and a Capture object, which is commutative with the previous definition
Base.:+(x::Number, T::Capture) = T + x

# Addition of a Capture object and a Vector, ensuring matching lengths
function Base.:+(T::Capture, x::Vector{<:Real})
    if length(T) != length(x)
        throw(ArgumentError("Vector must have the same length as the Capture object to be added."))
    end
    return Capture(T.val + x, T.sample_rate, T.time_start, T.stimuli)
end

# Since addition is commutative, the addition of a Vector and a Capture object can reuse the same implementation
Base.:+(x::Vector{<:Real}, T::Capture) = T + x


# Subtraction of two Capture objects, ensuring matching sample rates and lengths
function Base.:-(T::Capture, x::Capture)
    if T.sample_rate != x.sample_rate || length(T) != length(x)
        throw(ArgumentError("Capture objects must have the same sample rate and length to be subtracted."))
    end
    return Capture(T.val - x.val, T.sample_rate, T.time_start, T.stimuli)
end

# Subtraction of a Capture object and a Number, subtracting the Number to each element of the Capture's val
function Base.:-(T::Capture, x::Number)
    return Capture(T.val .- x, T.sample_rate, T.time_start, T.stimuli)
end

function Base.:-(x::Number, T::Capture)
    return Capture(x .- T.val, T.sample_rate, T.time_start, T.stimuli)
end

# Subtraction of a Capture object and a Vector, ensuring matching lengths
function Base.:-(T::Capture, x::Vector{<:Real})
    if length(T) != length(x)
        throw(ArgumentError("Vector must have the same length as the Capture object to be subtracted."))
    end
    return Capture(T.val - x, T.sample_rate, T.time_start, T.stimuli)
end

function Base.:-(x::Vector{<:Real}, T::Capture)
    if length(T) != length(x)
        throw(ArgumentError("Vector must have the same length as the Capture object to be subtracted."))
    end
    return Capture(x-T.val , T.sample_rate, T.time_start,  T.stimuli)
end

# Multiplication of two Capture objects, ensuring matching sample rates and lengths
function Base.:*(T::Capture, x::Capture)
    if T.sample_rate != x.sample_rate || length(T) != length(x)
        throw(ArgumentError("Capture objects must have the same sample rate and length to be added."))
    end
    return Capture(T.val .* x.val, T.sample_rate, T.time_start,  T.stimuli)
end

# Multiplication of a Capture object and a Number, multiplying the Number with each element of the Capture's val
function Base.:*(T::Capture, x::Number)
    return Capture(T.val .* x, T.sample_rate, T.time_start,  T.stimuli)
end

# Multiplication of a Number and a Capture object, which is commutative with the previous definition
Base.:*(x::Number, T::Capture) = T * x

# Element-wise Multiplication of a Capture object and a Vector, ensuring matching lengths
function Base.:*(T::Capture, x::Vector{<:Real})
    if length(T) != length(x)
        throw(ArgumentError("Vector must have the same length as the Capture object for element-wise multiplication."))
    end
    return Capture(T.val .* x, T.sample_rate, T.time_start,  T.stimuli)
end

# Since multiplication is commutative, the multiplication of a Vector and a Capture object can reuse the same implementation
Base.:*(x::Vector{<:Real}, T::Capture) = T * x

# Division of two capture object, dividing each element of the Capture's val by the the other capture
function Base.:/(T::Capture, x::Capture)
    if T.sample_rate != x.sample_rate || length(T) != length(x)
        throw(ArgumentError("Capture objects must have the same sample rate and length to be Divided."))
    end
    return Capture(T.val ./ x.val, T.sample_rate, T.time_start,  T.stimuli)
end


# Division of a Capture object by a Number, dividing each element of the Capture's val by the Number
function Base.:/(T::Capture, x::Number)
    return Capture(T.val ./ x, T.sample_rate, T.time_start,  T.stimuli)
end

function Base.:/(x::Number, T::Capture)
    return Capture(x ./ T.val, T.sample_rate, T.time_start,  T.stimuli)
end

# Element-wise Division of a Capture object by a Vector, ensuring matching lengths
function Base.:/(T::Capture, x::Vector{<:Real})
    if length(T) != length(x)
        throw(ArgumentError("Vector must have the same length as the Capture object for element-wise division."))
    end
    return Capture(T.val ./ x, T.sample_rate, T.time_start,  T.stimuli)
end

# Element-wise Division of a Capture object by a Vector, ensuring matching lengths
function Base.:/(x::Vector{<:Real}, T::Capture)
    if length(T) != length(x)
        throw(ArgumentError("Vector must have the same length as the Capture object for element-wise division."))
    end
    return Capture(x ./ T.val, T.sample_rate, T.time_start,  T.stimuli)
end


Base.:^(T::Capture, x::Number) = Capture(T.val .^ x, T.sample_rate, T.time_start, T.stimuli)
Base.:sqrt(T::Capture) = Capture(sqrt.(T.val), T.sample_rate, T.time_start, T.stimuli)

function Base.abs(T::Capture)
    return Capture(abs.(T.val), T.sample_rate, T.time_start, T.stimuli)
end

function qd_filt(coeff, T::Capture)
    return Capture(DSP.filt(coeff, T.val), T.sample_rate, T.time_start, T.stimuli)
end

function qd_matrix_mult(mat, T::Capture)
    return Capture(mat * T.val, T.sample_rate, T.time_start, T.stimuli)
end

function Base.angle(T::Capture)
    return Capture(angle.(T.val), T.sample_rate, T.time_start, T.stimuli)
end
function cov(x::Capture; kwargs...)
    Statistics.cov(x.val; kwargs...)
end

function cov(x::Capture, y::Capture; kwargs...)
    Statistics.cov(x.val, y.val; kwargs...)
end


function cor(x::Capture, y::Capture; kwargs...)
    Statistics.cor(x.val, y.val; kwargs...)
end

function autocor(x::Capture, lags::AbstractVector{<:Integer}; demean::Bool=true)
    StatsBase.autocor(x.val,lags::AbstractVector{<:Integer}; demean=demean)
end

function crosscor(x::Capture, y::Capture, lags::AbstractVector{<:Integer}; demean::Bool=true)
    StatsBase.crosscor(x.val, y.val, lags::AbstractVector{<:Integer}; demean=demean)
end

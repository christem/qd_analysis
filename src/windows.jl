function get_window_function(window_name::Symbol, N::Integer)
    if window_name == :Rectangular
        return RectangularWindow(N)
    elseif window_name == :Hanning
        return HanningWindow(N)
    elseif window_name == :Hamming
        return HammingWindow(N)
    elseif window_name == :Blackman
        return BlackmanWindow(N)
    elseif window_name == :BlackmanHarris
        return BlackmanHarrisWindow(N)
    else
        throw(ArgumentError("Invalid window name: $window_name"))
    end
end

#Define common window functions:
function RectangularWindow(N::Integer)
    return ones(N)
end

function HanningWindow(N::Integer)
    n = collect(0:N-1)
    return 0.5 .- 0.5 .* cos.(2*pi*n/(N-1))
end

function HammingWindow(N::Integer)
    n = collect(0:N-1)
    return 0.54 .- 0.46 .* cos.(2*pi*n/(N-1))
end

function BlackmanWindow(N::Integer)
    n = collect(0:N-1)
    return 0.42 .- 0.5 .* cos.(2*pi*n/(N-1)) .+ 0.08 .* cos.(4*pi*n/(N-1))
end

function BlackmanHarrisWindow(N::Integer)
    n = collect(0:N-1)
    return 0.35875 .- 0.48829 .* cos.(2*pi*n/(N-1)) .+ 0.14128 .* cos.(4*pi*n/(N-1)) .- 0.01168 .* cos.(6*pi*n/(N-1))
end
using DSP
using FFTW
using LinearAlgebra
using JLD2
using JSON3
using TensorCast
using Interpolations
using FileIO
using JLD2
using StructArrays

include("waveforms.jl")
include("capture_obj.jl")




"""
    generic_split(T::TimeDomain, split_size::Int, stride::Int)::Vector{TimeDomain}

Split an `object` object into sub- objects of a given dimension.

# Arguments
- `T`: The object to be split.
- `split_size`: The size of each sub-object.
- `stride`: The stride (or overlap) between successive splits.

# Returns
A vector of objects, each of size `split_size`. If the `split_size` is not a multiple 
of the length of the object, the object will be truncated 
to the nearest multiple of the `split_size`.

# Examples
```julia-repl
td = TimeDomain(1:10, rand(10), rand(10), rand(10), 1.0, 100.0)
split_td = split(td, 3, 1)
"""
function generic_split(T, split_size::Int, stride::Int; reverse_truncate::Bool=false)
    n = length(T)
    indices = 1:stride:n
    result = typeof(T)[]

    if reverse_truncate
        T = reverse(T)
    end

    for i in indices
        if i + split_size - 1 > n
            break
        end
        if reverse_truncate
            push!(result, reverse(T[i:i+split_size-1]))
        else
            push!(result, T[i:i+split_size-1])
        end
    end
    if reverse_truncate
        result = reverse(result)
    end
    return result
end

function Base.split(K::Capture{T}, split_size::Int, stride::Int; reverse_truncate::Bool=false) where T
    return generic_split(K, split_size, stride, reverse_truncate=reverse_truncate)
end
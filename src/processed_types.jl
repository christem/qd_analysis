# using LinearAlgebra
# using StructArrays
# using TensorCast
# using DocStringExtensions
# using Statistics
# using StatsBase
# using DSP

# include("waveforms.jl")

# abstract type ChannelProcessed end
# abstract type ResponseProcessed end

# """
#     TimeDomain(samples, rel_time, abs_time, val, gain, sample_rate)
# Encodes a time domain capture

# # Fields
# $(TYPEDFIELDS)
# """
# struct TimeDomain
#     "A vector of size N containing a list from 1...N where N is the number of samples."
#     samples::Vector{<:Integer}
#     "A vector of size N. Contains the relative time for each sample since the start of the capture."
#     rel_time::Vector{<:Real}  
#     "A vector of size N. Contains the absolute time for each sample."
#     abs_time::Vector{<:Real}
#     "A N Vector, containing the capture data. The data is normalized with respect to the ADC range."
#     val::Vector{<:Number}
#     "The gain factor of the capture"
#     gain::Real
#     "Sample rate of the capture"
#     sample_rate::Real
#     "Constructor check that the dimensions of the input vectors and matrices are consistent."
#     function TimeDomain(samples::Vector{<:Real}, rel_time::Vector{<:Real}, abs_time::Vector{<:Real}, val::Vector{<:Number}, gain::Real, sample_rate::Real)
#         if length(samples) != length(rel_time) || length(samples) != length(abs_time) || length(val) != length(samples) 
#             throw(DimensionMismatch("The dimensions of the input vectors and matrices are not consistent"))
#         end
#         new(samples, rel_time, abs_time, val, gain, sample_rate)
#     end
# end

# Base.size(T::TimeDomain) = (length(T.val), )
# Base.getindex(T::TimeDomain, i::Int) = TimeDomain([T.samples[i]], [T.rel_time[i]], [T.abs_time[i]], [T.val[i]], T.gain, T.sample_rate)
# Base.getindex(T::TimeDomain, I) = TimeDomain(T.samples[I], T.rel_time[I], T.abs_time[I], T.val[I], T.gain, T.sample_rate)
# Base.length(T::TimeDomain) = prod(size(T))
# Base.:+(T::TimeDomain, x::TimeDomain) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val + x.val, T.gain, T.sample_rate)
# Base.:+(T::TimeDomain, x::Number) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val + x, T.gain, T.sample_rate)
# Base.:+(T::TimeDomain, x::Vector{<:Real}) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val + x, T.gain, T.sample_rate)
# Base.:-(T::TimeDomain, x::TimeDomain) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val - x.val, T.gain, T.sample_rate)
# Base.:-(T::TimeDomain, x::Number) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val - x, T.gain, T.sample_rate)
# Base.:-(T::TimeDomain, x::Vector{<:Real}) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val - x, T.gain, T.sample_rate)
# Base.:*(T::TimeDomain, x::TimeDomain) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val .* x.val, T.gain, T.sample_rate)
# Base.:*(T::TimeDomain, x::Number) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val * x, T.gain, T.sample_rate)
# Base.:*(T::TimeDomain, x::Vector{<:Real}) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val .* x, T.gain, T.sample_rate)
# Base.:/(T::TimeDomain, x::TimeDomain) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val ./ x.val, T.gain, T.sample_rate)
# Base.:/(T::TimeDomain, x::Number) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val / x, T.gain, T.sample_rate)
# Base.:/(T::TimeDomain, x::Vector{<:Real}) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val ./ x, T.gain, T.sample_rate)
# Base.:^(T::TimeDomain, x::Number) = TimeDomain(T.samples, T.rel_time, T.abs_time, T.val .^ x, T.gain, T.sample_rate)
# Base.:sqrt(T::TimeDomain) = TimeDomain(T.samples, T.rel_time, T.abs_time, sqrt.(T.val), T.gain, T.sample_rate)

# function Base.:(==)(T::TimeDomain, x::TimeDomain)
#     return T.val == x.val && T.gain == x.gain && T.sample_rate == x.sample_rate
# end

# function Base.reverse(T::TimeDomain)
#     return TimeDomain(reverse(T.samples), reverse(T.rel_time), reverse(T.abs_time), reverse(T.val), T.gain, T.sample_rate)
# end

# function TimeDomain(val::Vector{<:Real})
#     return TimeDomain(collect(1:length(val)), collect(1:length(val)), collect(1:length(val)), val, 1, 1)
# end

# function qd_filt(K, T::TimeDomain)
#     return TimeDomain(T.samples, T.rel_time, T.abs_time, DSP.filt(K,T.val), T.gain, T.sample_rate)
# end

# """
#     FrequencyResponse(frequency, response)
# Encodes a frequency domain capture

# # Fields
# $(TYPEDFIELDS)
# """
# struct FrequencyResponse
#     "A vector of size N containing a list of frequency bins (Hz) of the FFT."
#     frequency::Vector{<:Real} 
#     "A vector of size N, containing the frequency data."
#     response::Vector{<:Complex}
#     "Constructor check that the dimensions of the input vectors are consistent."
#     function FrequencyResponse(frequency::Vector{<:Real}, response::Vector{<:Complex})
#         if length(frequency) != length(response)
#             throw(DimensionMismatch("The dimensions of the input vectors are not consistent"))
#         end
#         new(frequency, response)
#     end
# end

# Base.size(T::FrequencyResponse) = (length(T.response), )
# Base.getindex(T::FrequencyResponse, i::Int) = FrequencyResponse([T.frequency[i]], [T.response[i]])
# Base.getindex(T::FrequencyResponse, I) = FrequencyResponse(T.frequency[I], T.response[I])
# Base.length(T::FrequencyResponse) = prod(size(T))
# function Base.:+(T::FrequencyResponse, x::FrequencyResponse) 
#     interpped_response = interp(x, T.frequency)
#     return FrequencyResponse(T.frequency, T.response + interpped_response)
# end

# function Base.:-(T::FrequencyResponse, x::FrequencyResponse) 
#     interpped_response = interp(x, T.frequency)
#     return FrequencyResponse(T.frequency, T.response - interpped_response)
# end

# function Base.:*(T::FrequencyResponse, x::FrequencyResponse) 
#     interpped_response = interp(x, T.frequency)
#     return FrequencyResponse(T.frequency, T.response .* interpped_response)
# end

# function Base.:*(T::FrequencyResponse, x::Number) 
#     return FrequencyResponse(T.frequency, T.response * x)
# end

# function Base.:/(T::FrequencyResponse, x::FrequencyResponse)
#     if length(T) == 1
#         if T.frequency[1] == x.frequency[1]
#             return FrequencyResponse(T.frequency, T.response ./ x.response)  
#         end
#     end
#     interpped_response = interp(x, T.frequency)
#     return FrequencyResponse(T.frequency, T.response ./ interpped_response)
# end

# """
#     STFT(frequency, time, response)
# Encodes a short time fourier transform

# # Fields
# $(TYPEDFIELDS)
# """
# struct STFT
#     "A vector of size N containing a list of frequency bins (Hz) of the STFT."
#     frequency::Vector{<:Real}
#     "A vector of size P containing a list of times (s) of the STFT."
#     time::Vector{<:Real} 
#     "A NxP matrix, containing the STFT data. P is the number of time bins, N is the number of frequency bins."
#     response::Matrix{<:Complex}
#     "Constructor check that the dimensions of the input vectors and matrices are consistent."
#     function STFT(frequency::Vector{<:Real}, time::Vector{<:Real}, response::Matrix{<:Complex})
#         if length(frequency) != size(response, 1) || length(time) != size(response, 2)
#             throw(DimensionMismatch("The dimensions of the input vectors and matrices are not consistent"))
#         end
#         new(frequency, time, response)
#     end
# end

# Base.getindex(T::STFT, idx::Int) = FrequencyResponse(T.frequency, T.response[:, idx])

# function Base.getindex(T::STFT, idx::UnitRange{Int64})
#     STFT(T.frequency, T.time[idx], T.response[:, idx])
# end


# Base.size(T::STFT) = (length(T.frequency), length(T.time))
# Base.length(T::STFT) = length(T.time)

# function Base.:/(T::STFT, x::FrequencyResponse) 
#     interpped_response = interp(x, T.frequency)
#     return STFT(T.frequency, T.time, T.response ./ interpped_response)
# end

# function isEqualTimeFreq(T::STFT, x::STFT)
#     return (T.frequency == x.frequency) && (T.time == x.time)
# end

# function Base.:/(T::STFT, x::STFT) 
#     return STFT(T.frequency, T.time, T.response ./ x.response)
# end

# function Base.:*(T::STFT, x::Number) 
#     return STFT(T.frequency, T.time, T.response * x)
# end

# function Base.:+(T::STFT, x::STFT) 
#     return STFT(T.frequency, T.time, T.response .+ x.response)
# end

# function _response_from_frequency_responses(frequency_responses::Vector{FrequencyResponse})
#     # Extract frequency bins from the first FrequencyResponse object
#     frequency = frequency_responses[1].frequency
    
#     # Initialize a matrix to store the response data from all FrequencyResponse objects
#     response = Matrix{Complex{Float64}}(undef, length(frequency), length(frequency_responses))
    
#     # Populate the response matrix with data from the FrequencyResponse objects
#     for (t_idx, fr) in enumerate(frequency_responses)
#         # Check that the frequency bins are consistent across all FrequencyResponse objects
#         if fr.frequency != frequency
#             throw(ArgumentError("Inconsistent frequency bins in FrequencyResponse objects"))
#         end
#         # Assign the response data to the corresponding column of the matrix
#         response[:, t_idx] = fr.response
#     end
    
#     return response
# end

# function STFT(frequency_responses::Vector{FrequencyResponse}, time_bin_size::Real)
#     if isempty(frequency_responses)
#         throw(ArgumentError("Input vector of FrequencyResponse is empty"))
#     end

#     frequency = frequency_responses[1].frequency
#     time = collect(time_bin_size .* (0:length(frequency_responses)-1))
#     response = _response_from_frequency_responses(frequency_responses)

#     return STFT(frequency, time, response)
# end

# function STFT(frequency_responses::Vector{FrequencyResponse}, time::Vector{<:Real})
#     if isempty(frequency_responses)
#         throw(ArgumentError("Input vector of FrequencyResponse is empty"))
#     end
    
#     if length(frequency_responses) != length(time)
#         throw(DimensionMismatch("Length of provided time vector does not match number of FrequencyResponse objects"))
#     end

#     frequency = frequency_responses[1].frequency
#     response = _response_from_frequency_responses(frequency_responses)

#     return STFT(frequency, time, response)
# end

# """
#     Excitation(waveform_orig, waveform_triggered, gain)

# Encodes the state of the excitation waveform at the time of the capture

# # Fields
# $(TYPEDFIELDS)
# """
# struct Excitation
#     "Original Waveform uploaded to the DAC driver"
#     waveform_orig::Waveform
#     "Waveform where phase been corrected according to the trigger index when the acquisition was initiated."
#     waveform_triggered::Waveform
#     "Gain to be applied to the waveform to get the correct amplitude in volts" 
#     gain::Real
# end

# function Base.:(==)(T::Excitation, x::Excitation)
#     return T.waveform_orig == x.waveform_orig && T.waveform_triggered == x.waveform_triggered && T.gain == x.gain
# end

# """
#     Capture(response, waveform_triggered, waveform_orig)
# Encodes a response capture of a waveform

# # Fields
# $(TYPEDFIELDS)
# """
# struct Capture
#     "Time domain capture of the response measured across the DUT"
#     response::TimeDomain
#     "Excitation signal applied to the DUT"
#     excitation::Excitation
# end

# function Base.:(==)(T::Capture, x::Capture)
#     return T.response == x.response && T.excitation == x.excitation
# end

# Base.size(T::Capture) = (length(T.response), )
# Base.length(T::Capture) = length(T.response)
# Base.:+(T::Capture, x::Capture) = Capture(T.response + x.response, T.excitation)
# Base.:+(T::Capture, x::Number) = Capture(T.response + x, T.excitation)
# Base.:+(T::Capture, x::Vector{<:Real}) = Capture(T.response + x, T.excitation)
# Base.:+(T::Capture, x::TimeDomain) = Capture(T.response + x, T.excitation)
# Base.:-(T::Capture, x::Capture) = Capture(T.response - x.response, T.excitation)
# Base.:-(T::Capture, x::Number) = Capture(T.response - x, T.excitation)
# Base.:-(T::Capture, x::Vector{<:Real}) = Capture(T.response - x, T.excitation)
# Base.:-(T::Capture, x::TimeDomain) = Capture(T.response - x, T.excitation)
# Base.:*(T::Capture, x::Capture) = Capture(T.response * x.response, T.excitation)
# Base.:*(T::Capture, x::Number) = Capture(T.response * x, T.excitation)
# Base.:*(T::Capture, x::Vector{<:Real}) = Capture(T.response * x, T.excitation)
# Base.:*(T::Capture, x::TimeDomain) = Capture(T.response * x, T.excitation)
# Base.:/(T::Capture, x::Capture) = Capture(T.response / x.response, T.excitation)
# Base.:/(T::Capture, x::Number) = Capture(T.response / x, T.excitation)
# Base.:/(T::Capture, x::Vector{<:Real}) = Capture(T.response / x, T.excitation)
# Base.:/(T::Capture, x::TimeDomain) = Capture(T.response / x, T.excitation)
# Base.:^(T::Capture, x::Number) = Capture(T.response ^ x, T.excitation)
# Base.:sqrt(T::Capture) = Capture(sqrt(T.response), T.excitation)




# function Base.getindex(T::Capture, I)
#     new_timedomain = T.response[I]
#     new_exciation = Excitation(T.excitation.waveform_orig, update_phase_for_sample(T.excitation.waveform_triggered, new_timedomain.samples[1]-1), T.excitation.gain)
#     return Capture(new_timedomain, new_exciation)
# end

# Base.lastindex(T::Capture) = length(T)
# Base.firstindex(T::Capture) = 1

# #Maybe we should do something with T.excitation here
# function Base.reverse(T::Capture)
#     return Capture(reverse(T.response), T.excitation)
# end

# ## Todo: Extend beyond K::ZeroPoleGain{:z, ComplexF64, ComplexF64, Float64}
# function qd_filt(K, T::Capture)
#     return Capture(qd_filt(K, T.response), T.excitation)
# end

# function stft_to_frequencyResponse(stft::STFT)
#     @cast responses[i] |= FrequencyResponse(Ref(impedance_spectrum.frequency), impedance_spectrum.response[:,i])
#     return responses.(stft.frequency, stft.response)
# end

# function cov(x::Capture; kwargs...)
#     cov(x.response; kwargs...)
# end

# function cov(x::TimeDomain; kwargs...)
#     Statistics.cov(x.val; kwargs...)
# end

# function cov(x::Capture, y::Capture; kwargs...)
#     cov(x.response, y.response; kwargs...)
# end

# function cov(x::TimeDomain, y::TimeDomain; kwargs...)
#     Statistics.cov(x.val, y.val; kwargs...)
# end

# function cor(x::Capture, y::Capture; kwargs...)
#     cor(x.response, y.response; kwargs...)
# end

# function cor(x::TimeDomain, y::TimeDomain; kwargs...)
#     Statistics.cor(x.val, y.val; kwargs...)
# end

# function autocor(x::Capture, lags::AbstractVector{<:Integer}; demean::Bool=true)
#     autocor(x.response,lags::AbstractVector{<:Integer}; demean=demean)
# end

# function autocor(x::TimeDomain, lags::AbstractVector{<:Integer}; demean::Bool=true)
#     StatsBase.autocor(x.val,lags::AbstractVector{<:Integer}; demean=demean)
# end

# function crosscor(x::Capture, y::Capture, lags::AbstractVector{<:Integer}; demean::Bool=true)
#     crosscor(x.response, y.response, lags::AbstractVector{<:Integer}; demean=demean)
# end

# function crosscor(x::TimeDomain, y::TimeDomain, lags::AbstractVector{<:Integer}; demean::Bool=true)
#     StatsBase.crosscor(x.val, y.val, lags::AbstractVector{<:Integer}; demean=demean)
# end

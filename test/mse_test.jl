using Test
using StructArrays
using qd_analysis

@testset "frequency response mse" begin
    capture = random_capture(10, 1000, 16000, 0, 0, 1, 1, 400000)
    a = extract_frequency_response(capture)
    a_resp = copy(a.response)
    a_resp[1] = a_resp[1] + 2
    b = FrequencyResponse(a.frequency, a_resp)
    c = mse(a, b)
    @test isapprox(c,0.4)
end


@testset "frequency response mse" begin
    capture = random_capture(10, 1000, 16000, 0, 0, 1, 1, 400000)
    a = extract_frequency_response(capture)
    b = extract_periodogram(capture, 1000)
    b_resp = b.response
    b_resp[1,5] = b_resp[1,5] + 2
    b = STFT(b.frequency, b.time, b_resp)
    c = mse(a, b)
    for i in 1:4
        @test isapprox(c[i],0.0, atol=1e-9)
    end
    @test isapprox(c[5],0.4)
    for i in 6:10
        @test isapprox(c[i],0.0, atol=1e-9)
    end
end
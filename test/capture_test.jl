using Test
using qd_analysis


@testset "Capture Constructor" begin
    a = Capture([1])
    @test a.val == [1]
end

@testset "Capture Overloads" begin
    a = Capture([1])
    b = Capture([2])
    #Addition
    c = a + b
    @test c.val == [3]
    c = a + 1
    @test c.val == [2]
    c = 1 + a
    @test c.val == [2]
    c = a + [1]
    #Subtraction
    @test c.val == [2]
    c = a - b
    @test c.val == [-1]
    c = a - 1
    @test c.val == [0]
    c = 1 - a
    @test c.val == [0]
    c = a - [1]
    @test c.val == [0]
    #Multiplication
    c = a * b
    @test c.val == [2]
    c = a * 2
    @test c.val == [2]
    c = 2 * a
    @test c.val == [2]
    c = a * [1]
    @test c.val == [1]
    #Division
    c = a / b
    @test c.val == [0.5]
    c = a / 2
    @test c.val == [0.5]
    c = 2 / a
    @test c.val == [2]
    c = a / [1]
    @test c.val == [1]
end

@testset "Capture with Multisine Waveform" begin
    a = Capture(Multisine([0.5], total_length=4, phases=[4]), normalize=false)
    @test all(isapprox.(a.val,  [cos(4), cos(4 + π), cos(4), cos(4 + π)]))
end

@testset "Capture indexing" begin
    cap = Capture(Multisine([0.1, 0.2], total_length=1001, phases=[4, 0]), normalize=false)

    @test cap[5:10].sample_rate == 1.0
    @test cap[5:2:10].sample_rate == 0.5

    @test length(cap[5:10]) == 6

    @test cap[5:10].time_start == 4

end
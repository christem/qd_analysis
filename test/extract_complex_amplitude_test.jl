using Test
using StructArrays
using qd_analysis


@testset "frequency response" begin
    sineWave = Multisine([0.01, 0.02, 0.05, 0.1], total_length=100000)
    cap = Capture(sineWave)
    cap = cap + ones(length(cap))
    a, coef = extract_frequency_response(cap, noise_signal= ones(length(cap)))
    @test all(isapprox.(abs.(a.response), 0.25))
    @test all(isapprox.(angle.(a.response), 0,atol=1e-7))
end


@testset "periodogram" begin
    sineWave = Multisine([0.01, 0.02, 0.05, 0.1], total_length=100000)
    cap = split(Capture(sineWave), 10000, 10000)
    periodogram = extract_frequency_response(cap)
    @test all(isapprox.(abs.(periodogram.response), 0.25))
    @test all(isapprox.(angle.(periodogram.response), 0,atol=1e-9))
end

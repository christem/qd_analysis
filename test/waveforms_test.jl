using Test
using StructArrays
using qd_analysis

sine_wave_1 = Sine(0.1, π/2, 1)
sine_wave_2 = Sine(0.2, 1, 1)
sine_wave_3 = Sine(0.3, 2, 1)
sine_wave_4 = Sine(0.4, 3, 1)
sine_wave_5 = Sine(0.5, 4, 1)
multisine_1 = Multisine(StructVector([sine_wave_1, sine_wave_2, sine_wave_3, sine_wave_4, sine_wave_5]), 100)
multisine_2 = Multisine(StructVector([sine_wave_1, sine_wave_2, sine_wave_3, sine_wave_4, sine_wave_5]), 101)


@testset "Equality operators" begin
    @test Sine(0.1, π/2, 1) == Sine(0.1, π/2, 1)
    @test Sine(0.1, π/2, 1) != Sine(0.2, π/4, 1)
    
    multisine_2 = Multisine(StructVector([sine_wave_1, sine_wave_2]), 100)
    multisine_3 = Multisine(StructVector([sine_wave_1, sine_wave_2]), 100)
    multisine_4 = Multisine(StructVector([sine_wave_3, sine_wave_4]), 50)
    
    @test multisine_2 == multisine_3
    @test multisine_2 != multisine_4
end

@testset "Multisine Constructor" begin
    a = Multisine([0.1, 0.2, 0.3, 0.4, 0.5], phases=[π/2, 1, 2, 3, 4]);
    @test multisine_1.sines == a.sines
    @test a.total_length == 30
    @test is_periodic(a) == true
end

@testset "Sine struct" begin
    @test sine_wave_1.frequency == 0.1
    @test sine_wave_1.phase == π/2
    @test sine_wave_1.amplitude == 1
end

@testset "get_phase_at_sample function" begin
    trigger_phase = get_phase_at_sample(sine_wave_1, 0)
    @test trigger_phase ==  π/2

    trigger_phase = get_phase_at_sample(sine_wave_1, 1)
    @test trigger_phase == π/2 + 2π * sine_wave_1.frequency

    multisine_3 = update_phase_for_sample(multisine_1, 1)
    @test multisine_3.sines[1].phase == π/2 + 2π * sine_wave_1.frequency
    @test multisine_3.sines[2].phase == 1 + 2π * sine_wave_2.frequency
    @test multisine_3.sines[3].phase == 2 + 2π * sine_wave_3.frequency
    @test multisine_3.sines[4].phase == 3 + 2π * sine_wave_4.frequency
    @test multisine_3.sines[5].phase == mod(4 + 2π * sine_wave_5.frequency,2π)
end

@testset "Multisine struct" begin
    @test length(multisine_1.sines) == 5
    @test multisine_1.total_length == 100
end

@testset "is_periodic function" begin
    @test is_periodic(sine_wave_1, 12) == false
    @test is_periodic(sine_wave_2, 100) == true
    @test is_periodic(multisine_1) == true
    @test is_periodic(multisine_2) == false
end

@testset "waveform_to_array function" begin
    @test all(isapprox.(waveform_to_array(sine_wave_5, 4), [cos(4), cos(4 + π), cos(4), cos(4 + π)]))
    @test waveform_to_array(multisine_1, len=1, normalize=true) == [-1]
    @test waveform_to_array(multisine_1, len=1, normalize=false) == [cos(π/2)+cos(1)+cos(2)+cos(3)+cos(4)]
    @test waveform_to_array(multisine_1, len=2, normalize=false) == [-1.5194806481430598, cos(pi/2+0.1*2π)+cos(1+0.2*2π)+cos(2+0.3*2π)+cos(3+0.4*2π)+cos(4+0.5*2π)]
    @test waveform_to_array(multisine_1, len=2, normalize=true) == [-1, (cos(pi/2+0.1*2π)+cos(1+0.2*2π)+cos(2+0.3*2π)+cos(3+0.4*2π)+cos(4+0.5*2π))/1.5194806481430598]
end

using Test
using StructArrays
using qd_analysis
using DSP
using LinearAlgebra
using Statistics

nsampl = 10000
noise = randn(nsampl)

@testset "wiener base" begin
    samples = collect(1:nsampl)
    omega = 0.1
    
    # Phase shift on the line
    phase_shift_1 = π / 4
    # Amplitude shift on the line
    amplitude_1 = 1
    # Phase shift for line noise measurement
    phase_shift_2 = π / 6
    # Amplitude shift for line noise measurement
    amplitude_2 = 0.1
    
    # Generate the original sine wave signal
    s = sin.(omega * samples)
    s_1 = amplitude_1 * sin.(omega * samples .+ phase_shift_1)
    s_2 = amplitude_2 * sin.(omega * samples .+ phase_shift_2)
    
    u = s
    y = s_1 .+ noise
    y2 = s_2 .+ 2 * noise
    
    
    w = wiener_coeff_multichannel(u, [y y2], 5) # You need to implement or include the wiener_coeff_multichannel function
    u_est = filt(w[:,1], y) + filt(w[:,2], y2) # Adjust to the filter function you are using
    error_variance_noisy = mean((u - y) .^ 2)
    error_variance_est = mean((u - u_est) .^ 2)
    println("Error variance of noisy signal: $(error_variance_noisy)")
    println("Error variance of estimated signal: $(error_variance_est)") # Uncomment when u_est is defined

    @test error_variance_est < 0.01

end

# @testset "wiener timedomain" begin
#     samples = collect(1:nsampl)
#     omega = 0.1
    
#     # Phase shift on the line
#     phase_shift_1 = π / 4
#     # Amplitude shift on the line
#     amplitude_1 = 1
#     # Phase shift for line noise measurement
#     phase_shift_2 = π / 6
#     # Amplitude shift for line noise measurement
#     amplitude_2 = 0.1

    
#     # Generate the original sine wave signal
#     s = sin.(omega * samples)
#     s_1 = amplitude_1 * sin.(omega * samples .+ phase_shift_1)
#     s_2 = amplitude_2 * sin.(omega * samples .+ phase_shift_2)
    
#     u = TimeDomain(s)
#     y = TimeDomain(s_1 .+ noise)
#     y2 = TimeDomain(s_2 .+ 2 * noise)

    
    
#     w = wiener_coeff_multichannel(u, [y,y2], 5) # You need to implement or include the wiener_coeff_multichannel function
#     u_est = qd_filt(w[:,1], y) + qd_filt(w[:,2], y2) # Adjust to the filter function you are using
#     error_variance_noisy = mean((u.val - y.val) .^ 2)
#     error_variance_est = mean((u.val - u_est.val) .^ 2)
#     println("Error variance of noisy signal: $(error_variance_noisy)")
#     println("Error variance of estimated signal: $(error_variance_est)") # Uncomment when u_est is defined
#     @test error_variance_est < 0.01

# end

@testset "wiener Capture" begin
    omega = 0.1/(2*pi)
    
    # Phase shift on the line
    phase_shift_1 = (π / 4) -(π/2)
    # Amplitude shift on the line
    amplitude_1 = 1
    # Phase shift for line noise measurement
    phase_shift_2 = (π / 6) - (π/2)
    # Amplitude shift for line noise measurement
    amplitude_2 = 0.1

    
    # Generate the original sine wave signal

    u = Capture(Multisine([omega], total_length = nsampl, amplitudes = [1], phases=[-pi/2]), normalize = false)
    y = Capture(Multisine([omega], total_length = nsampl, amplitudes = [amplitude_1], phases=[phase_shift_1]), normalize = false)
    y2 = Capture(Multisine([omega], total_length = nsampl, amplitudes = [amplitude_2], phases=[phase_shift_2]), normalize = false)

    y = y + noise
    y2 = y2 + 2 * noise
    
    w = wiener_coeff_multichannel(u, [y,y2], 5) # You need to implement or include the wiener_coeff_multichannel function
    u_est = qd_filt(w[:,1], y) + qd_filt(w[:,2], y2) # Adjust to the filter function you are using
    error_variance_noisy = mean((u.val - y.val) .^ 2)
    error_variance_est = mean((u.val - u_est.val) .^ 2)
    println("Error variance of noisy signal: $(error_variance_noisy)")
    println("Error variance of estimated signal: $(error_variance_est)") # Uncomment when u_est is defined
    @test error_variance_est < 0.01

end

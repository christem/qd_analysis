using Test
using StructArrays
using qd_analysis

@testset "fft timedomain test" begin
    sineWave = Multisine(StructVector([Sine(0.1, 0, 1)]),10)
    randomTimeDomain = Capture(sineWave)


    fftTimeDomain = single_sided_fft(randomTimeDomain, window=:Rectangular)

    @test length(fftTimeDomain.frequency) == 6
    @test length(fftTimeDomain.response) == 6
    @test fftTimeDomain.frequency[1] == 0
    @test fftTimeDomain.frequency[end] == 0.5
    @test abs(fftTimeDomain.response[2]) == 1

end


@testset "fft split capture test" begin
    sineWave = Multisine(StructVector([Sine(0.1, 0, 1)]),100)
    capture = Capture(sineWave)
    captures = split(capture, 10, 10)

    fftCaptures = single_sided_fft(captures, window=:Rectangular)

    @test typeof(fftCaptures) == STFT
    @test size(fftCaptures) == (6,10)
    @test abs.(fftCaptures.response) ≈ Float64.([0 0 0 0 0 0 0 0 0 0;
                                         1 1 1 1 1 1 1 1 1 1;
                                         0 0 0 0 0 0 0 0 0 0;
                                         0 0 0 0 0 0 0 0 0 0;
                                         0 0 0 0 0 0 0 0 0 0;
                                         0 0 0 0 0 0 0 0 0 0])
end

@testset "fft of gapped capture" Base.byte_string_classify
    sineWave = Multisine(StructVector([Sine(0.1, 0, 1)]),100)
    cap = Capture([1,2,3,4,5,NaN,8,9,10], 1, 0, sineWave)


    fftCaptures = single_sided_fft(cap, window=:Rectangular, fft_to_return=:all)

    amplitude_capture = extract_frequency_response(cap, amplitudes_to_return=:all)

end
using Test
using StructArrays
using qd_analysis

@testset "split time domain overlap" begin
    cap = Capture(Multisine([0.1, 0.2], total_length=1001, phases=[4, 0]), normalize=false)
    splitTimeDomain = split(cap, 100, 50)
    splitTimeDomainReverse = split(cap, 100, 50, reverse_truncate=true)

    @test length(splitTimeDomain) == 19
    # test that all splits are the correct length
    @test all([length(x) == 100 for x in splitTimeDomain])
    #test that the abs time fits
    @test all([t.time_start == (idx-1)*50 for (idx, t) in enumerate(splitTimeDomain)])
    # test that the last/first 50 samples overlap between splits
    @test all([x.val[51:end] == splitTimeDomain[i+1].val[1:50] for (i,x) in enumerate(splitTimeDomain[1:end-1])])
    # test that the first split is the first 100 samples
    @test splitTimeDomain[1].val == cap.val[1:100]

    # test that the last split is the last 100 samples (truncated)
    @test splitTimeDomain[end].val == cap.val[end-100:end-1]

    #Make sure that gain and sample rate are preserved for all splits
    @test all([x.sample_rate == cap.sample_rate for x in splitTimeDomain])

    # Test that splitTimeDomain and splitTimeDomainReverse are off by one sample
    @test all([x.val[2:end] == y.val[1:end-1] for (x,y) in zip(splitTimeDomain, splitTimeDomainReverse)])

end

@testset "split time domain no overlap" begin 
    cap = Capture(Multisine([0.1, 0.2], total_length=1001, phases=[4, 0]), normalize=false)
    splitTimeDomain = split(cap, 100, 100)

    @test length(splitTimeDomain) == 10
    # test that all splits are the correct length
    @test all([length(x) == 100 for x in splitTimeDomain])
    # test that there is no overlap between splits and that they contigiously cover the entire time domain
    @test all([x.val == cap.val[(1+100*(i-1)):(100*i)] for (i,x) in enumerate(splitTimeDomain)])

    #Make sure that gain and sample rate are preserved for all splits
    @test all([x.sample_rate == cap.sample_rate for x in splitTimeDomain])
end
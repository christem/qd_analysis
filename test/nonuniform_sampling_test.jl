using Test
using StructArrays
using qd_analysis

@testset "nonUniform" begin
    sineWave = Multisine(StructVector([Sine(0.1, 0, 1)]),1000)
    sineCapture_orig = Capture(sineWave)

    fftTimeDomain = single_sided_fft(sineCapture_orig, window=:Rectangular)
    @test isapprox(maximum(fftTimeDomain),1.0 ; atol=0.01)
    @test argmax(fftTimeDomain) == 101

    sineCapture_missing_sample = Capture([sineCapture_orig.val[collect(1:250)]; sineCapture_orig.val[collect(252:750)]; sineCapture_orig.val[collect(752:1000)]])
    sampleCapture = Capture([collect(1:250); collect(252:750); collect(752:1000)])

    fftTimeDomain_missing_sample = single_sided_fft(sineCapture_missing_sample, window=:Rectangular)

    @test !(isapprox(maximum(fftTimeDomain_missing_sample),1.0 ; atol=0.01))

    resampled = nonuniform_to_uniform_sampling(sineCapture_missing_sample, sampleCapture)

    fftTimeDomain_resampled = single_sided_fft(resampled, window=:Rectangular)
    @test isapprox(maximum(fftTimeDomain_resampled), 1.0; atol=0.01)

end
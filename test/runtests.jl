using qd_analysis
using Test

@testset "qd_analysis.jl" begin

include("waveforms_test.jl")
include("variable_generator_test.jl")
include("split_test.jl")

end

using Revise
using qd_analysis
using Documenter

DocMeta.setdocmeta!(qd_analysis, :DocTestSetup, :(using qd_analysis); recursive=true)

makedocs(;
    modules=[qd_analysis],
    authors="Magnus Christensen <magnus.christensen@cern.ch> and contributors",
    repo="https://gitlab.cern.ch/christem/qd_analysis.jl/blob/{commit}{path}#{line}",
    sitename="qd_analysis.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md"
        "Waveforms" => "waveforms.md" 
        "Acquisition" => "acquisition.md"
        "Processing" => "processing.md"
        "Processed Types" => "processed_types.md"
    ],
)

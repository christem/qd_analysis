# Waveforms

## Sine
```@docs
Sine
get_phase_at_sample
```

## Multisine
```@docs
Multisine
is_periodic
```


## Common Functions


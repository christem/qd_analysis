# Processed Types
Denotes types returned from higher-level processing calls. To be easily interfaced with plotting


```@docs
TimeDomain
FrequencyResponse
STFT
Excitation
Capture
```

 